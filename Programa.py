# Clase principal del programa
from Services.PreparationService import PreparationService
from Controllers.HomeController import HomeController
from Router import Router


class Programa:

    def __init__(self):
        self.preparationService = PreparationService()
        self.homeController = HomeController()
        self.router = Router()
        self.opcion = 0

    def inicio(self):
        self.preparationService.prepararSistema()
        self.loop()

    def loop(self):
        while self.opcion != 9:
            self.homeController.menuPrincipal()
            try:
                self.opcion = int(input("Ingrese una opcion: "))
                self.router.llamarOpcion(self.opcion)
            except:
                self.opcion = 0
        print()
        print("Gracias por venir.")
        print()