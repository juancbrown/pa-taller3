# Clase de soporte para el manejo de rutas, ayuda a determinar que método en que controlador se debe de invocar.
from Controllers.HomeController import HomeController
from Controllers.OperadorController import OperadorController
from Controllers.ConsumoController import ConsumoController
from Services.PreparationService import PreparationService


class Router:

    def __init__(self):
        self.name = "Router"
        self.homeController = HomeController()
        self.operadorController = OperadorController()
        self.consumoController = ConsumoController()
        self.preparationService = PreparationService()


    def llamarOpcion(self,opcion):
        opciones = {
            1 : self.homeController.inicio,
            9 : self.preparationService.prepararSistema
        }
        accion = opciones.get(opcion, lambda: "Opcion incorrecta")
        accion()