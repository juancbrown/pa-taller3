# Servicio para interactuar con el archivo consumo.csv
from Services.DatabaseService import DatabaseService
from Services.FileService import FileService

class ConsumoService:

    def __init__(self):
        self.databaseService = DatabaseService()
        self.fileService = FileService()

    def leerConsumos(self):
        #TODO jcbrown:jcbrown Implementar la lógica para estraer todos los consumos del CSV
        return {
            1: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12},
            2: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12},
            3: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12},
            4: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12}
        }

    def leerCiudades(self):
        #TODO jcbrown:jcbrown Implementar la lógica para extraer todas las ciudades del CSV
        return {1,2,3}

    def leerConsumoCiudad(self, ciudad):
        #TODO jcbrown:jcbrown Implementar la lógica para leer los consumos de una sola ciudad
        return {
            1: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12},
            2: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12},
            3: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12},
            4: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12}
        }

    def leerConsumoHora(self, hora):
        #TODO jcbrown:jcbrown Implementar la lógica para leer los consumos de una sola hora
        return {
            1: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12},
            2: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12},
            3: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12},
            4: { "Ciudad" : 1, "Consumo" : 142.5, "Hora": 12}
        }

    def leerTotalConsumo(self):
        #TODO jcbrown:jcbrown Implementar la lógica para totalizar los consumos
        return {
            1: { "Ciudad" : 1, "TotalConsumo" : 142.5, "HoraPico": 12},
            2: { "Ciudad" : 1, "TotalConsumo" : 142.5, "HoraPico": 12},
            3: { "Ciudad" : 1, "TotalConsumo" : 142.5, "HoraPico": 12},
            4: { "Ciudad" : 1, "TotalConsumo" : 142.5, "HoraPico": 12}
        }
