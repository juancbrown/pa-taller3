# Servicio de preparacion del sistema. Ejecuta una serie de verificaciones al iniciar el sistema.
from Services.ConfigurationService import ConfigurationService
from Services.DatabaseService import DatabaseService
from Services.ConsumoService import ConsumoService


class PreparationService:

    def __init__(self):
        _config = ConfigurationService
        _databaseService = DatabaseService


    def prepararSistema(self):
        print("Inicializando el sistema...")
        self.__verificarArchivoConsumo()
        self.__verificarBaseDatos()
        self.__verificarTablaOperador()
        self.__verificarTablaEstaciones()
        self.__verificarOperador()
        print("Sistema Inicializado....")

    def __verificarBaseDatos(self):
        return True

    def __verificarTablaOperador(self):
        return True

    def __verificarTablaEstaciones(self):
        return True

    def __verificarArchivoConsumo(self):
        return True

    def __verificarOperador(self):
        return True

