# Servicio para interactuar con la base de datos
from Services.ConfigurationService import ConfigurationService

class DatabaseService:

    def __init__(self):
        self.configurationService = ConfigurationService()

    def existeBaseDatos(self):
        #TODO jcbrown:jcbrown Implementar Lógica para verificar si la base de datos existe.
        return True

    def crearBaseDatos(self):
        #TODO jcbrown:jcbrown Implementar lógica para crear base de datos.
        return True

    def existeTabla(self, nombreTabla):
        #TODO jcbrown:jcbrown Implementar la lógica necesaria para verificar la existencia de la tabla
        return True

    def crearTabla(self, estructura):
        #TODO jcbrown:jcbrown Implementar la lógica necesaria para crear la tabla.
        return True