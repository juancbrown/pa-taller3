# Taller 3
Desarrollado por Juan Carlos Brown Aguilar para el curso de programación avanzada.

## Requisitos generales
El sistema utliza una base de datos SQLite y un Archivo CSV con informacion de consumo.

## Detalles de los requerimientos
El documento de requerimientos está disponible en formato PDF dentro de la carpeta Docs.