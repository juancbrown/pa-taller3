# Controlador para manejar lo relacionado al operador

from Services.DatabaseService import DatabaseService
from Views.OperadorViews import OperadorViews


class OperadorController:

    def verificarOperador(self):
        databaseService = DatabaseService
        operadorExisteView = OperadorViews
        if databaseService.existeOperador():
            operadorExisteView.operadorExiste()
        else:
            operadorExisteView.operadorNoExiste()
