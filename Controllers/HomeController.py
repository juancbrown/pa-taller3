# Controlador Principal de inicio del programa
from Views.HomeViews import HomeViews

class HomeController:

    def __init__(self):
        self.homeView = HomeViews()

    # Carga pantalla inicial
    def inicio(self):
        self.homeView.viewInicio()

    def menuPrincipal(self):
        self.homeView.menuPrincipal()