# Controlador que maneja todos los procesos relacionados al consumo
from Services.ConsumoService import ConsumoService
from Views.ConsumoViews import ConsumoViews

class ConsumoController:

    def __init__(self):
        self.consumoService = ConsumoService()
        self.consumoViews = ConsumoViews()