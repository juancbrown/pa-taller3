# Vista para mostrar en caso de que ya exista un operador registrado en el sistema.


class OperadorViews:

    def operadorExiste(self):
        print("Ya existe un operador registrado en el sistema.")
        print("No es necesario registrar un nuevo operador.")

    def operadorNoExiste(self):
        print("No hay operadores registrados.")
        print("Debe de registrar un operador en el sistema.")
