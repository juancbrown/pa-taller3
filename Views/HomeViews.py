# Vista principal de inicio


class HomeViews:

    def viewInicio(self):
        print()
        print('Bienvenido al sistema')
        print()

    def menuPrincipal(self):
        print()
        print()
        print("----- MENU DE OPCIONES -----")
        print("1 - Ver datos por ciudad.")
        print("2 - Ver datos por hora.")
        print("3 - Ver todos los datos.")
        print("4 - Ver Reporte.")
        print("5 - Generar Reporte.")
        print("6 - Cambiar Nombre de la Subestacion.")
        print("7 - Cambiar Operador.")
        print("8 - Verificar sistema.")
        print("9 - Salir.")
